# nftworlds-query
This project intends to show people the historic images of a given world. 

## Requirements
This is built using the following: 
* Flask
* Lambda/AWS Serverless Application Model (build framework)
* Gitlab for CI - Probably optional

You could probably upload the repo straight into lambda and it would work fine. It would probably also run fine without Lambda. 

To go down the CI route, you need: 
* AWS Acces Key/Secret added to the "variables" in the GitLab repo
* s3 bucket to store static files and logs
* Alchemy API Key your AWS Param Store (It's pulled as an environment variable via template.yaml as part of the SAM build)
* Configure .gitlab-ci.yml as desired - I've got it to execute the runner upon pushing to master

To run this locally:
* AWS SAM CLI
* Docker (Recommended)
* aws cli (Optional - Nice to have to do quick s3 uploads)
* Add your Alchemy API key to env.json
* To build, run `sam build --use-container` (Container not required, but replicates how lambda/gitlab runner works so eliminates inconsistencies)
* To run locally run `sam local start-api --env-vars env.json --warm-containers EAGER` 

## Notes
There are a few worlds have errors, these are mostly the historic "hacked" worlds. This is because when you query their tokenURI as part of the contract, it returns "Scheduled Maintenance". I'm sure this can be fixed rather than writing a workaround.
https://api.nftworlds.com/ipfs?ipfs=QmUu8PjLTNNqAGXVUa68UqxprLiYRidwtofChEgaobwvPf

`s3 sync` is probably better than `s3 cp` because apparently sync only updates if the file has changed.

early_update_and_mints.json is generated by another script, and shouldn't need to be generated again. High level of the script: 
* Get all txns for the contract
* Filter on update transactions
* Filter on mint transactions
* For update transactions that don't have 'lastMetadataIPFSHash', create a chronological list of ipfs hashes
* Insert the mint transaction to the start of the list 

To add more static content to s3, you need to specific allow public acls in the template.yaml (Or create a policy).

I suck at HTML and CSS and basically I gave up. I almost spent more time trying to get a fucking image inline with text than writing the logic...

The timeout in template.yaml probably needs to be increased if running locally. I can't understand why my decent PC can take so long.

## Goals:
* Make something simple but useful
* Learn some AWS inc. lambda
* Learn a pipeline (gitlab-ci)
* Learn Flask
* Fail at CSS and HTML
