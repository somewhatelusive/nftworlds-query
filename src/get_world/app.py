import os
import json
import requests
from web3 import Web3
from flask import Flask, jsonify, render_template
from requests.structures import CaseInsensitiveDict

app = Flask(__name__)

# These should be stored as secure strings, but I'm a tight arse and don't want to pay for a KMS key - especially given it's a free key
alchemyapi = os.environ.get('ALCHEMYAPI')

# Contract of the nftworlds token
contract = "0xbd4455da5929d5639ee098abfaa3241e9ae111af" 

# Load the file now so we don't have to load it multiple times (not sure if this is more of less efficient)
with open('early_update_and_mints.json') as json_file:
    mints = json.load(json_file)
early_worlds = list(map(lambda x : x['world'], mints))

# Setup web3 config for interacting with token functions later
w3 = Web3(Web3.HTTPProvider('https://eth-mainnet.alchemyapi.io/v2/' + alchemyapi))
abi = [{"inputs":[{"internalType":"uint256","name":"_mintSupplyCount","type":"uint256"},{"internalType":"uint256","name":"_ownerMintReserveCount","type":"uint256"},{"internalType":"uint256","name":"_whitelistExpirationTimestamp","type":"uint256"},{"internalType":"uint256","name":"_maxWhitelistCount","type":"uint256"},{"internalType":"uint256","name":"_maxMintPerAddress","type":"uint256"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"owner","type":"address"},{"indexed":True,"internalType":"address","name":"approved","type":"address"},{"indexed":True,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"owner","type":"address"},{"indexed":True,"internalType":"address","name":"operator","type":"address"},{"indexed":False,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":True,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"from","type":"address"},{"indexed":True,"internalType":"address","name":"to","type":"address"},{"indexed":True,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"_ipfsGateway","type":"string"}],"name":"emergencySetIPFSGateway","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"getBiomes","outputs":[{"internalType":"string[]","name":"","type":"string[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"getDensities","outputs":[{"internalType":"string[3]","name":"","type":"string[3]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"getFeatures","outputs":[{"internalType":"string[]","name":"","type":"string[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"getGeography","outputs":[{"internalType":"uint24[5]","name":"","type":"uint24[5]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"getResources","outputs":[{"internalType":"uint16[9]","name":"","type":"uint16[9]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"getSeed","outputs":[{"internalType":"int32","name":"","type":"int32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"ipfsGateway","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes","name":"_signature","type":"bytes"}],"name":"joinWhitelist","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"mintEnabled","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"mintSupplyCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"components":[{"internalType":"uint256","name":"_tokenId","type":"uint256"},{"internalType":"int32","name":"_seed","type":"int32"},{"components":[{"internalType":"uint24[5]","name":"geographyData","type":"uint24[5]"},{"internalType":"uint16[9]","name":"resourceData","type":"uint16[9]"},{"internalType":"uint8[3]","name":"densities","type":"uint8[3]"},{"internalType":"uint8[]","name":"biomes","type":"uint8[]"},{"internalType":"uint8[]","name":"features","type":"uint8[]"}],"internalType":"struct NFTWorlds.WorldData","name":"_worldData","type":"tuple"},{"internalType":"string","name":"_tokenMetadataIPFSHash","type":"string"}],"internalType":"struct NFTWorlds.MintData","name":"_mintData","type":"tuple"},{"internalType":"bytes","name":"_signature","type":"bytes"}],"name":"mintWorld","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bool","name":"_enabled","type":"bool"}],"name":"setMintEnabled","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"index","type":"uint256"}],"name":"tokenByIndex","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"tokenMetadataIPFSHashes","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"uint256","name":"index","type":"uint256"}],"name":"tokenOfOwnerByIndex","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalMinted","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_tokenId","type":"uint256"},{"internalType":"string","name":"_tokenMetadataIPFSHash","type":"string"}],"name":"updateMetadataIPFSHash","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"whitelistAddressCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"whitelistMintCount","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]
contract_address = Web3.toChecksumAddress(contract)
read_contract = w3.eth.contract(address=contract_address, abi=abi)

def fixNameDesc(ipfs_metadata):
    # Purpose: Not all metadata has a worldName or worldDescription so let's set it here 
    # Input: metadata dictionary 
    # Output: metadata dictionary 
    try:
        if "worldName" not in ipfs_metadata:
            ipfs_metadata['worldName'] = ipfs_metadata['name']
        if "worldDescription" not in ipfs_metadata:
            ipfs_metadata['worldDescription'] = ""
    except:
        print("Issue correcting metadata. Value of input: {}".format(ipfs_metadata))
        ipfs_metadata = {"error": "unable to get metadata", "external_url": "not found"}
    # We don't use the attributes dictionary so drop it
    ipfs_metadata.pop('attributes',None)
    return ipfs_metadata

def getMetadata(url, world_id):
    # Purpose: HTTP get from ipfs url
    # Input: URL
    # Output: metadata dictionary 
    headers = CaseInsensitiveDict()
    headers["Accept"] = "application/json"
    resp = requests.get(url) 
    ipfs_metadata = []
    try:
        ipfs_metadata = fixNameDesc(resp.json())
    except:
        print("Error getting ipfs metadata at: {}\nResult: {}".format(url, resp.text))
        ipfs_metadata = {'external_url': 'https://www.nftworlds.com/{}'.format(world_id), 'image': 'https://www.nftworlds.com/static/media/world-grid.eddcf038.png', 'name': 'World #{}'.format(world_id), 'worldName': 'World #{}'.format(world_id), 'worldDescription': 'Error getting correct image'}
    return ipfs_metadata

def getCurrentIPFS(world_id):
    # Purpose: Get the current ipfs metadata from the contract
    # Input: world_id (string)
    # Output: List of metadata

    ipfs_list = []
    ipfs_list.append(getMetadata(read_contract.functions.tokenURI(int(world_id)).call(),world_id))
    return ipfs_list

def checkEarlyUpdaters(ipfs_list):
    # Purpose: Append ipfs metadata for set list of worlds updated before lastMetadataIPFSHash. Source: early_update_and_mints.json
    # Input: List of metadata ( Originally this was a dict, but I believe that's wrong - see logic error notes )
    # Output: List of metadata
    
    last_ipfs = ipfs_list[-1] # Other side of the logic error from getWorldList - I believe this is fixed now

    # Create a unique list of world_ids to check against
    early_updated_list = list(map(lambda x : x['world'], mints))
    
    # Check if the input is in our early_updated_list 
    confirmed_early_updater = [world_id for world_id in early_updated_list if(str(world_id) in last_ipfs['external_url'])]
    
    ipfs_metadata_list = []
    # There has to be a better way to do this... My brain broke
    # If the world is in our known list of worlds added before lastMetadataIPFSHash existed, refer to json and append a list of ipfs_metadata
    # Check ever world in our early worlds list until it matches external_url
    if confirmed_early_updater:
        for world_id in early_worlds:
            world_id = str(world_id)
            if world_id in last_ipfs['external_url']:
                ipfs_metadata_list = next((item['ipfs_metadata'] for item in mints if str(item['world']) == world_id), None)

    # We need to reverse this list as these are ascending chronologically, but the rest of the list is decending. We fix this right at the end
    ipfs_metadata_list.reverse()
    return ipfs_metadata_list


def getWorldList(ipfs_list):
    # Purpose: Recursively generate a list of worlds tracing back using lastMetadataIPFSHash
    # Input: List of metadata
    # Output: List of metadata
    last_ipfs = ipfs_list[-1]
    if 'lastMetadataIPFSHash' in last_ipfs:
        # If the most recent metadata has lastMetadataIPFSHash, get the new metadata and recurse
        ipfs_list.append(getMetadata("https://ipfs.nftworlds.com/ipfs/{}".format(last_ipfs['lastMetadataIPFSHash']),""))
        getWorldList(ipfs_list)
    else:
        # If the most recent metadata does not have lastMetadataIPFSHash, check against our list of early updaters
        #early_updaters = checkEarlyUpdaters(last_ipfs) # I believe this was is a logic error 
        early_updaters = checkEarlyUpdaters(ipfs_list) 
        #mint_ipfs = checkEarlyUpdaters(ipfs_list)
        if early_updaters:
            # If there are results, add them to the list
            ipfs_list = ipfs_list + early_updaters
        return ipfs_list
    # Not 100% sure how recursion works, this return should never be reached I suppose
    return ipfs_list

# Useful for offline testing:
# ipfs_list = getWorldList(getCurrentIPFS(8217))

# Integrate with flask/lambda
@app.route('/')
def lambda_handler(message, context):
    
    # If we get a bad request, go away
    if ('pathParameters' not in message or
            message['httpMethod'] != 'GET'):
        return {
            'statusCode': 400,
            'headers': {},
            'body': json.dumps({'msg': 'Bad Request'})
        }
    # Pull world id from url
    world_id = message['pathParameters']['id']

    try:
        check_id = int(world_id)
    except:
        print("Error: Non numeric world_id. Returning 400")
        return {
            'statusCode': 400,
            'headers': {},
            'body': json.dumps({'msg': 'Bad Request: Non numeric world_id'})
        }

    
    if check_id not in range(1, 10001):
        print("Error: Invalid world_id. Returning 400")
        return {
            'statusCode': 400,
            'headers': {},
            'body': json.dumps({'msg': 'Bad Request: Invalid world_id'})
        }
    print("Request for {}".format(world_id))

    # Execute magic
    ipfs_list = getWorldList(getCurrentIPFS(world_id))
    
    # Reverse the list so we have ascending chronological order
    ipfs_list.reverse()

    # Send to flask template with a list of ipfs metadata
    with app.app_context():
        body = render_template('index.html', paths=ipfs_list, world=world_id)

    # Display content
    return {        
        "statusCode": 200,
        "headers": {
            "Content-Type": 'text/html',
        },
        "body": body
    }


